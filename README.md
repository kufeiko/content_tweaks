# Content tweaks

Some comments on the tasks.

## Task 1

This can be done with Paragraphs module.

## Task 2

This is done by hooks in  content_tweaks.module file by checking if current user has the administrator role.

## Tasks 3, 4, 5

These got kinda combined.

To enable using of the weather data in multiple places, I've created a field formatter that can be applied to a text field and placed across different entities. The field itself holds the post code, but the formatter displays the weather status. To display both, the post code and the weather to the user, there are two ways: 

* print the values directly in the .twig

* create a pseudo field that will be then presented in the 'Manage Display' form.

Some remarks on these tasks:

* The weather service that is in use has some problems with post codes; it works OK for Bulgaria and the US, but not for UK or Germany. Therefore I hardcoded London, UK in the request.
* Ideally, the weather data service should be injected; I didn't do it for the sake of time.
* Caching for anonymous users. D8 will not automatically invalidate the cache for anonymous users and the weather data will stay the same until a node is re-saved. To counter this, either the Internal Page Cache module has to be disabled (leaving the cache work to Dynamic Page Cache module) or the weather widget should be populated via ajax.