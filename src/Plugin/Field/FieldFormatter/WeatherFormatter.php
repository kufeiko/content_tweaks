<?php

namespace Drupal\content_tweaks\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'weather_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "weather_formatter",
 *   module = "content_tweaks",
 *   label = @Translation("Weather formatter"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class WeatherFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();
     
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        'weather' => $this->getWeaterByPostCode(1000),
      ];
    }
     
    return $elements;
  }

  /**
   * Get weather data form REST API endpoint.
   * @param  string $postCode A postcode
   * @return array           render array
   */
  protected function getWeaterByPostCode($postCode) {
    $api_key = 'd67298062bc95ebec5584831984222cf';
    $endpoint = 'https://api.openweathermap.org/data/2.5/weather';

    // Cache the response -- for test purposes, 1 minute only.
    $build = [
      '#cache' => [
        'max-age' => 60,
      ],
    ];

    // Well, turns out OWM has some problems with postcodes in the UK.
    // We'll just use London as a location.
    try {
      $response = \Drupal::httpClient()->request('GET', $endpoint, [
        'query' => [
          'q' => 'London,uk',
          'APPID' => $api_key,
        ],
      ]);
      $data = $response->getBody()->getContents();
      $decoded = json_decode($data);

      if (!$decoded) {
        throw new \Exception('Invalid data returned from API');
      }
    }
    catch (\Exception $e) {
      return $build;
    }

    $build['weather'] = [
      '#type' => 'item',
      '#markup' => $decoded->weather[0]->main . " (cached on " . date("Y-m-d H:i:s") . ")",
    ];

    return $build;
  }
}
